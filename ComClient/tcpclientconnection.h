#ifndef TCPCLIENTCONNECTION_H
#define TCPCLIENTCONNECTION_H

#include <QObject>
#include <QTcpSocket>
#include <QNetworkInterface>


class tcpClientConnection : public QObject
{
    Q_OBJECT

public:
    explicit tcpClientConnection(QObject *parent = 0, QString name="Default", QHostAddress ip = QHostAddress::LocalHost, int port = 0);
    void writeToServer(QString data);
private slots:
    void dataReceived();
signals:
    void dataOk(QString data);
private:
    QTcpSocket *_tcpSoc;
    QString ID = "AA-00001";



};

#endif // TCPCLIENTCONNECTION_H
