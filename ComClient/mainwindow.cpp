#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->ip->setText("192.168.20.184");
    ui->port->setText("8000");
    ui->name->setText("AA-00001");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connect_clicked()
{
    QHostAddress address = QHostAddress(ui->ip->text());
    int port = ui->port->text().toInt();
    QString ID = ui->name->text();
    _client = new tcpClientConnection(this,ID,address,port);
    ui->ip->setReadOnly(true);
    ui->port->setReadOnly(true);
    ui->name->setReadOnly(true);
    ui->connect->setEnabled(false);
    connect(_client, SIGNAL(dataOk(QString)),this, SLOT(addLine(QString)));
}

void MainWindow::on_send_clicked()
{
    _client->writeToServer(ui->data->text());
}

void MainWindow::addLine(QString data)
{
    ui->textData->append(data);
}
