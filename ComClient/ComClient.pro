#-------------------------------------------------
#
# Project created by QtCreator 2016-05-21T13:59:26
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ComClient
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tcpclientconnection.cpp

HEADERS  += mainwindow.h \
    tcpclientconnection.h

FORMS    += mainwindow.ui
