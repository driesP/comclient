#include "tcpclientconnection.h"

tcpClientConnection::tcpClientConnection(QObject *parent,QString name,QHostAddress ip, int port)
{
    ID = name;
    _tcpSoc = new QTcpSocket(parent);
    _tcpSoc->connectToHost(ip, port);
    connect(_tcpSoc, SIGNAL(readyRead()), this, SLOT(dataReceived()));
    if(_tcpSoc->waitForConnected())
    {
        QByteArray byteArray(ID.toStdString().c_str());
        _tcpSoc->write(byteArray);
        _tcpSoc->flush();
    }
}

void tcpClientConnection::writeToServer(QString data)
{
    QString sendString = ID + ":"+ data;
    QByteArray temp(sendString.toStdString().c_str());
    _tcpSoc->write(temp);
    _tcpSoc->flush();
}
void tcpClientConnection::dataReceived()
{
    unsigned long long bytesAvailable = _tcpSoc->bytesAvailable();
    char buf[bytesAvailable];
    _tcpSoc->read(buf,bytesAvailable);
    QString dataRecv;
    for (int i = 0; i < bytesAvailable; i++)
    {
        dataRecv.append(buf[i]);
    }

    emit dataOk(dataRecv);
}
